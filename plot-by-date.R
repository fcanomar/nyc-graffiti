plotmap_m <- function(month){
  coord_month <- graf_br[year(graf_br$CREATED_DATE)==2015 & month(graf_br$CREATED_DATE)==month,]
  coord_month <- coord_month[!is.na(coord_month$x) & !is.na(coord_month$y),]
  coordinates(coord_month) <- c("x","y")
  plot(nyc_br)
  plot(coord_month,add=TRUE,pch=20)
}

plotmap_dm <- function(day,month){
  coord_month <- graf_br[year(graf_br$CREATED_DATE)==2015 & month(graf_br$CREATED_DATE)==month & day(graf_br$CREATED_DATE)==day ,]
  coord_month <- coord_month[!is.na(coord_month$coord_x),]
  if (nrow(coord_month)==0) {return("No se hicieron Graffitis en este día")}
  coordinates(coord_month) <- c("x","y")
  plot(nyc_br)
  plot(coord_month,add=TRUE,pch=20)
}

getcoord <- function(day,month,year){
  coord <- subset(graf_br,year(graf_br$CREATED_DATE)==year & month(graf_br$CREATED_DATE)==month & day(graf_br$CREATED_DATE)==day,na.rm=T)
  #coord <- coord[!is.na(coord_month$coord_x) & !is.na(coord_month$coord_y),c("coord_x","coord_y")]
  if (nrow(coord_month)==0) {return(NULL)}
  coordinates(coord) <- c("x","y")
  return(coord)
}

#
#para ir añadiendo por día...
#plot(getcoord(26,3,2015),add=T)
#añadir función que a partir de un mapa y un día inicial vaya añadiendo por día??


plotmapgg <- function(coord){
  autoplot(nyc_br, expand=FALSE) + geom_point(data=coord,aes(x,y, fill= ..level..), alpha =0.5, geom="polygon")
}

